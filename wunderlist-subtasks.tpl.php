<?php

/**
 * @file
 * Template file to display subtasks.
 */
?>
<div class="wunderlist-subtasks">
<?php foreach ($subtasks as $subtask): ?>
  <div class="wunderlist-subtask">
    <?php if ($subtask['completed']): ?>
      <span class="subtask-completed">V</span>
    <?php endif; ?>
    <h4><?php print $subtask['title']; ?></h4>
    created at: <?php print $subtask['created_at']; ?>
  </div>
<?php endforeach; ?>
</div>
